export const localhost = "localhost:8080";

export const appRoutes = {
    search: "/search",
    trialPlotPage: "/trial-plot"
}